-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: cms
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additional_setting`
--

DROP TABLE IF EXISTS `additional_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `additional_setting` (
  `id_additional_setting` int(11) NOT NULL AUTO_INCREMENT,
  `key_add` text DEFAULT NULL,
  `value_add` text DEFAULT NULL,
  PRIMARY KEY (`id_additional_setting`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `additional_setting`
--

LOCK TABLES `additional_setting` WRITE;
/*!40000 ALTER TABLE `additional_setting` DISABLE KEYS */;
INSERT INTO `additional_setting` VALUES (1,'city_id','255'),(2,'province_id','11'),(3,'province','Jawa Timur'),(4,'city','Kabupaten Malang'),(5,'type','Kabupaten'),(6,'postal_code','65162'),(9,'jenis_pengiriman','[{\"text\":\"POS Indonesia\",\"val\":\"pos\"},{\"text\":\"JNE\",\"val\":\"jne\"},{\"text\":\"Tiki\",\"val\":\"tiki\"}]'),(10,'jenis_pembayaran','[{\"text\":\"Bank BCA\",\"no_rek\":\"12321312312312\",\"atas_nama\":\"Rendy Yani Susanto\"},{\"text\":\"Bank BRI\",\"no_rek\":\"43432\",\"atas_nama\":\"Cicik Winarsih\"},{\"text\":\"Bank BNI\",\"no_rek\":\"56566\",\"atas_nama\":\"Rendy Yani Susanto\"}]');
/*!40000 ALTER TABLE `additional_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `id_blog_category` int(11) NOT NULL AUTO_INCREMENT,
  `blog_category` text DEFAULT NULL,
  PRIMARY KEY (`id_blog_category`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (3,'Berita'),(4,'Kegiatan'),(5,'Produk');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_post`
--

DROP TABLE IF EXISTS `blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_post` (
  `id_blog_post` int(11) NOT NULL AUTO_INCREMENT,
  `title` text DEFAULT NULL,
  `tag` text DEFAULT NULL,
  `blog_post` text DEFAULT NULL,
  `img` text DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `idblogcategory_fk` int(11) DEFAULT NULL,
  `visit_count` int(255) DEFAULT NULL,
  PRIMARY KEY (`id_blog_post`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_post`
--

LOCK TABLES `blog_post` WRITE;
/*!40000 ALTER TABLE `blog_post` DISABLE KEYS */;
INSERT INTO `blog_post` VALUES (3,'Omicron Bikin Malaysia Tangguhkan Umrah Mulai 8 Januari ','berita, terkini','<p>Jakarta - Malaysia menangguhkan perjalanan umrah ke Arab Saudi imbas lonjakan kasus Corona dari mereka yang datang dari negara tersebut. Terlebih data menunjukkan ada potensi penyebaran varian Omicron di antara jemaah.<br />\r\nSeperti dilansir The Star, Minggu (2/1/2022) Kementerian Kesehatan Malaysia menangguhkan perjalanan umrah ke Arab Saudi mulai 8 Januari 2022 mendatang sampai waktu yang belum bisa ditentukan. Jemaah yang tiba selepas umrah di Malaysia juga harus menjalani karantina di pusat karantina atau hotel yang ditunjuk oleh Badan Nasional Penanggulangan Bencana (Nadma) mulai Senin (3/1).<br />\r\n<br />\r\n<br />\r\n<img alt=\"\" src=\"/cms/include/kcfinder/upload/images/clayton-malquist-P2iaN5Kqk-4-unsplash.jpg\" style=\"height:200px; width:300px\" /><br />\r\nBaca artikel detiknews, &quot;Omicron Bikin Malaysia Tangguhkan Umrah Mulai 8 Januari&quot; selengkapnya&nbsp;<a href=\"https://news.detik.com/internasional/d-5880585/omicron-bikin-malaysia-tangguhkan-umrah-mulai-8-januari\">https://news.detik.com/internasional/d-5880585/omicron-bikin-malaysia-tangguhkan-umrah-mulai-8-januari</a>.<br />\r\n<br />\r\nDownload Apps Detikcom Sekarang https://apps.detik.com/detik/</p>\r\n','4f817c9ae8a4a125ae1dec0de0688530.png','2022-01-02 03:40:02',3,0),(4,'SMK IT Asy-Syadzili menjuarai Sakssi dalam bidang smart school','juara, smk','<p>hay, smk it menjuarai smart school loh&nbsp;Seperti dilansir The Star, Minggu (2/1/2022) Kementerian Kesehatan Malaysia menangguhkan perjalanan umrah ke Arab Saudi mulai 8 Januari 2022 mendatang sampai waktu yang belum bisa ditentukan. Jemaah yang tiba selepas umrah di Malaysia juga harus menjalani karantina di pusat karantina atau hotel yang ditunjuk oleh Badan Nasional Penanggulangan Bencana (Nadma) mulai Senin (3/1).</p>\r\n\r\n<p><img alt=\"\" src=\"/cms/include/kcfinder/upload/images/KTP%20A.%20Daffa.jpg\" style=\"height:100%; width:100%\" /></p>\r\n','51847c083f27bda35e8fcf81999a57a3.jpg','2022-01-02 09:46:07',4,0),(6,'YUK TENGOK SERUNYA KEGIATAN PBB YANG DILAKUKAN SETELAH APEL MPLS','juara, smk','<p>Banjari merupakan seni penggabungan antara musik dengan keregilian selain itu banjari banyak di gemari oleh masyarakat muda khususnya di jatim Dan pada tanggal 14 Maret 2020 SMK IT ASY-SYADZILI mendapatkan gelar juara 2 tingkat jatim dengan persiapan waktu kurang lebih seminggu ini sangat mengejutkan dan yang saat itu di gelar di SMA NEGERI 3 MALANG</p>\r\n','37955ae4455b6938631822cda238f027.jpeg','2022-01-05 09:52:17',3,0);
/*!40000 ALTER TABLE `blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fasilitas`
--

DROP TABLE IF EXISTS `fasilitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fasilitas` (
  `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  PRIMARY KEY (`id_fasilitas`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fasilitas`
--

LOCK TABLES `fasilitas` WRITE;
/*!40000 ALTER TABLE `fasilitas` DISABLE KEYS */;
INSERT INTO `fasilitas` VALUES (1,'Perpustakaan','perpus.jpg','Kami memiliki ruang perpustakaan yang luas dan memiliki banyak buku yang dapat menjadi sumber belajar siswa'),(2,'Kantin','kantin.jpg','Kami memiliki ruang perpustakaan yang luas dan memiliki banyak buku yang dapat menjadi sumber belajar siswa'),(3,'Koperasi','koperasi.jpg','Kami memiliki ruang perpustakaan yang luas dan memiliki banyak buku yang dapat menjadi sumber belajar siswa'),(4,'Ruang Kelas','kelas.jpg','Kami memiliki ruang perpustakaan yang luas dan memiliki banyak buku yang dapat menjadi sumber belajar siswa'),(5,'UKS','uks.jpg','Kami memiliki ruang perpustakaan yang luas dan memiliki banyak buku yang dapat menjadi sumber belajar siswa');
/*!40000 ALTER TABLE `fasilitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galery`
--

DROP TABLE IF EXISTS `galery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galery` (
  `id_galery` int(11) NOT NULL AUTO_INCREMENT,
  `img` text DEFAULT NULL,
  `title` text DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_galery`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galery`
--

LOCK TABLES `galery` WRITE;
/*!40000 ALTER TABLE `galery` DISABLE KEYS */;
INSERT INTO `galery` VALUES (1,'galery1.png',NULL,NULL,'2021-12-20 02:39:25'),(2,'galery2.png',NULL,NULL,'2021-12-20 02:39:26'),(3,'galery3.png',NULL,NULL,'2021-12-20 02:39:27'),(4,'galery4.png',NULL,NULL,'2021-12-20 02:39:27'),(5,'galery5.png',NULL,NULL,'2021-12-20 02:39:28'),(6,'galery6.png',NULL,NULL,'2021-12-20 02:39:30');
/*!40000 ALTER TABLE `galery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'siswa','Siswa'),(3,'guru','Guru'),(4,'staff','Staff'),(5,'kepsek','kepsek'),(6,'kurikulum','kurikulum'),(7,'tu','Tata Usaha');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori_produk`
--

DROP TABLE IF EXISTS `kategori_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori_produk` (
  `id_kategori_produk` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_produk` text DEFAULT NULL,
  PRIMARY KEY (`id_kategori_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori_produk`
--

LOCK TABLES `kategori_produk` WRITE;
/*!40000 ALTER TABLE `kategori_produk` DISABLE KEYS */;
INSERT INTO `kategori_produk` VALUES (1,'Jajanan'),(2,'makanan'),(3,'minuman');
/*!40000 ALTER TABLE `kategori_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keunggulan`
--

DROP TABLE IF EXISTS `keunggulan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keunggulan` (
  `id_keunggulan` int(11) NOT NULL AUTO_INCREMENT,
  `keunggulan` text DEFAULT NULL,
  `icon` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  PRIMARY KEY (`id_keunggulan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keunggulan`
--

LOCK TABLES `keunggulan` WRITE;
/*!40000 ALTER TABLE `keunggulan` DISABLE KEYS */;
INSERT INTO `keunggulan` VALUES (1,'Pendidikan Berkualitas',NULL,'Sekolah terletak pada tempat  yang cukup strategis dekat  jalan  raya, Sekolah ini merupakan salah satu sekolah yang cukup diminati masyarakat sekitar, hal ini tampak dari jumlah siswa  yang cukup banyak'),(2,'Lingkungan Bersih',NULL,'Sekolah terletak pada tempat  yang cukup strategis dekat  jalan  raya, Sekolah ini merupakan salah satu sekolah yang cukup diminati masyarakat sekitar, hal ini tampak dari jumlah siswa  yang cukup banyak'),(3,'Fasilitas Lengkap',NULL,'Sekolah terletak pada tempat  yang cukup strategis dekat  jalan  raya, Sekolah ini merupakan salah satu sekolah yang cukup diminati masyarakat sekitar, hal ini tampak dari jumlah siswa  yang cukup banyak');
/*!40000 ALTER TABLE `keunggulan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id_media` int(11) NOT NULL AUTO_INCREMENT,
  `img` text DEFAULT NULL,
  `tag` text DEFAULT NULL,
  `title` text DEFAULT NULL,
  PRIMARY KEY (`id_media`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,'1.png',NULL,'1'),(2,'3.png',NULL,'3'),(3,'4.png',NULL,'4'),(4,'5.png',NULL,'5'),(5,'91b83a0188e13986037d70730a65daa8.jpg',NULL,'91b83a0188e13986037d70730a65daa8.jpg'),(6,'3ec268335bc319d64a8965dba7d92dd6.jpg',NULL,'3ec268335bc319d64a8965dba7d92dd6.jpg');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_files`
--

DROP TABLE IF EXISTS `media_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_files` (
  `id_media_files` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text DEFAULT NULL,
  `extension` text DEFAULT NULL,
  `url` text DEFAULT NULL,
  PRIMARY KEY (`id_media_files`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_files`
--

LOCK TABLES `media_files` WRITE;
/*!40000 ALTER TABLE `media_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `is_price` tinyint(4) DEFAULT NULL,
  `idkategoriproduk_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produk`
--

LOCK TABLES `produk` WRITE;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` VALUES (1,'Gedhang','<p>dasdajks</p>\r\n\r\n<p><img alt=\"\" src=\"/cms/include/kcfinder/upload/images/npwp.jpg\" style=\"height:239px; width:391px\" /></p>\r\n',10000,'4e8cd7a3166b7c827b1e8f9871602366.jpeg',1,1),(2,'Singkong Keju','<p><img alt=\"\" src=\"/cms/include/kcfinder/upload/images/KTP%20A.%20Daffa.jpg\" style=\"height:615px; width:993px\" />kjkjkoko</p>\r\n',1900000,'b976d1bc4b5f56ca364c0d947d8a7c36.png',1,2);
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profil_website`
--

DROP TABLE IF EXISTS `profil_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profil_website` (
  `nama_website` text CHARACTER SET latin1 DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 DEFAULT NULL,
  `no_hp` text CHARACTER SET latin1 DEFAULT NULL,
  `logo` text CHARACTER SET latin1 DEFAULT NULL,
  `icon` text CHARACTER SET latin1 DEFAULT NULL,
  `singkatan` text CHARACTER SET latin1 DEFAULT NULL,
  `tagline` text CHARACTER SET latin1 DEFAULT NULL,
  `about_us` text CHARACTER SET latin1 DEFAULT NULL,
  `email` text CHARACTER SET latin1 DEFAULT NULL,
  `pemilik` text CHARACTER SET latin1 DEFAULT NULL,
  `facebook` text CHARACTER SET latin1 DEFAULT NULL,
  `twitter` text CHARACTER SET latin1 DEFAULT NULL,
  `youtube` text CHARACTER SET latin1 DEFAULT NULL,
  `instagram` text CHARACTER SET latin1 DEFAULT NULL,
  `img_about_us` text CHARACTER SET latin1 DEFAULT NULL,
  `wa_text` text CHARACTER SET latin1 DEFAULT NULL,
  `background` text CHARACTER SET latin1 DEFAULT NULL,
  `credit_by` text CHARACTER SET latin1 DEFAULT NULL,
  `breadcrumb_elements` text CHARACTER SET latin1 DEFAULT NULL,
  `nama` text CHARACTER SET latin1 DEFAULT NULL,
  `kota` text CHARACTER SET latin1 DEFAULT NULL,
  `negara` text CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profil_website`
--

LOCK TABLES `profil_website` WRITE;
/*!40000 ALTER TABLE `profil_website` DISABLE KEYS */;
INSERT INTO `profil_website` VALUES ('SDN Lowokwaru 4','JL.SETAMAN NO 2 KELURAHAN LOWOKWARU','0341 -414596','7b07eeafb857cc330e6e745cae15920f.png','','SDN4Lowokwaru','#setaman (sehat,takwa dan mandiri)','<p><strong>TERWUJUDNYA GENERASI YANG BERKARAKTER, UNGGUL DALAM IMTAQ DAN IPTEK SERTA BERBUDAYA LINGKUNGAN</strong></p>\r\n','sdnlowokwaruempat@gmail.com','Ferry',' sdnlowokwaru 4','@sdnlowokwaru4','SDN Lowokwaru 4 Malang Official ','sdn_lowokwaru4_malang','a00ae551dd485419a57db462a6ae30b9.jpg','Halo Admin, Saya ingin bertanya/membeli/.....','d797c14a0070235734508b7d9f409395.jpg','&copy; 2019. <a href=\"#\">Core Master</a> by<a href=\"https://myber.co.id\" target=\"_blank\">Myber</a>','Core Master','SD NEGERI LOWOKWARU 4 KOTA MALANG','Malang','Indonesia');
/*!40000 ALTER TABLE `profil_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting_akun`
--

DROP TABLE IF EXISTS `setting_akun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting_akun` (
  `id_setting_akun` int(11) NOT NULL AUTO_INCREMENT,
  `kode` text DEFAULT NULL,
  `nama` text DEFAULT NULL,
  `debit` text DEFAULT NULL,
  `kredit` text DEFAULT NULL,
  PRIMARY KEY (`id_setting_akun`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting_akun`
--

LOCK TABLES `setting_akun` WRITE;
/*!40000 ALTER TABLE `setting_akun` DISABLE KEYS */;
/*!40000 ALTER TABLE `setting_akun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting_table`
--

DROP TABLE IF EXISTS `setting_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting_table` (
  `id_setting_table` int(11) NOT NULL AUTO_INCREMENT,
  `table` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `value` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  PRIMARY KEY (`id_setting_table`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting_table`
--

LOCK TABLES `setting_table` WRITE;
/*!40000 ALTER TABLE `setting_table` DISABLE KEYS */;
INSERT INTO `setting_table` VALUES (32,'enlight','head_post','Selamat Datang di SDN Lowokwaru 4 Malang',NULL),(33,'enlight','head_post_color','#e74c3c',NULL),(34,'enlight','head_post_text_col','#fff',NULL),(35,'enlight','text_news','Berita',NULL),(36,'enlight','color_news','#e74c3c',NULL);
/*!40000 ALTER TABLE `setting_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting_website`
--

DROP TABLE IF EXISTS `setting_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting_website` (
  `SPP` double(20,0) DEFAULT NULL,
  `DSP` double(20,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting_website`
--

LOCK TABLES `setting_website` WRITE;
/*!40000 ALTER TABLE `setting_website` DISABLE KEYS */;
INSERT INTO `setting_website` VALUES (0,NULL);
/*!40000 ALTER TABLE `setting_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tim`
--

DROP TABLE IF EXISTS `tim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tim` (
  `id_tim` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text DEFAULT NULL,
  `jabatan` text DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `panggilan` text DEFAULT NULL,
  PRIMARY KEY (`id_tim`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tim`
--

LOCK TABLES `tim` WRITE;
/*!40000 ALTER TABLE `tim` DISABLE KEYS */;
INSERT INTO `tim` VALUES (1,'Budhi Setyo Rini, S.Pd','Guru Kelas 1B','g1.jpg','We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.','Bu Rini'),(2,'Budhi Setyo Rini, S.Pd','Guru Kelas 1B','g2.jpg','We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.','Bu Rini'),(3,'Budhi Setyo Rini, S.Pd','Guru Kelas 1B','g3.jpg','We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.','Bu Rini'),(4,'Budhi Setyo Rini, S.Pd','Guru Kelas 1B','g4.jpg','We are a team of multi-skilled and curious digital specialists who are always up for a challenge and learning as fast as digital is changing.','Bu Rini');
/*!40000 ALTER TABLE `tim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `foto` text DEFAULT NULL,
  `anggota_id` int(11) DEFAULT NULL,
  `table` text DEFAULT NULL,
  `is_walas` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','admin','$2y$08$mxSDKMRKsKM3IwN6NPoB6.3pQqymDu4ZtGAMIzpw.ppcie3MJM9ty','','hello@admin.com','',NULL,NULL,NULL,1268889823,1641372274,1,'admin','Yani','ADMIN','085894632505','3c8f6f36f650d5ce07803470b4f4d4ff.jpg',NULL,NULL,NULL),(3,'127.0.0.1','rendy','$2y$08$mxSDKMRKsKM3IwN6NPoB6.3pQqymDu4ZtGAMIzpw.ppcie3MJM9ty','','hello@admin.com','',NULL,NULL,NULL,1268889823,1623404287,1,'Rendy','Yani','ADMIN','085894632505','3c8f6f36f650d5ce07803470b4f4d4ff.jpg',2,'guru',1),(4,'::1','is_376326','$2y$08$btIyb0KsgC.oQwjrEAo.kOtGP2v.HAIe6I7fXaGSi1D8ftrhDAKSK',NULL,'is_376326@gmail.com',NULL,NULL,NULL,NULL,1609057333,1622508531,1,'Abdur Rochim, S.Pd.',NULL,NULL,NULL,NULL,6,'guru',NULL),(5,'::1','is_559029','$2y$08$O0X.fpQqfSjoyvdnwkwMGuizSSh2UGVW65nYjY60F3QT4c0cEAU36',NULL,'is_559029@gmail.com',NULL,NULL,NULL,NULL,1609057451,1609141005,1,'Avi Hendratmoko, S.Kom.',NULL,NULL,NULL,NULL,7,'guru',NULL),(6,'::1','is_692692','$2y$08$IRAxfjkQv0dssEQI3d6CAeKGJyhxtcvM6jq66D2g681Oe4Z5sFB1S',NULL,'is_692692@gmail.com',NULL,NULL,NULL,NULL,1609057777,1609057789,1,'M. Maimun Muzakka',NULL,NULL,NULL,NULL,8,'guru',1),(7,'::1','is_591727','$2y$08$HRS9vvh3bShD9YQ6TJaaru7ypOsaB4yo8Eigjr0ueEK0A07h/H0Xa',NULL,'is_591727@gmail.com',NULL,NULL,NULL,NULL,1609057880,1609057922,1,'Cindy Permata Putri, S.Pd',NULL,NULL,NULL,NULL,9,'guru',1),(8,'::1','is_988975','$2y$08$wyknxv1J15BzDCWXjEF/hOj8wSYniCCfz/v9CmD6WCAiVTGX2OD.u',NULL,'is_988975@gmail.com',NULL,NULL,NULL,NULL,1609058009,1621504190,1,'Roikhatul Uzza, S.Psi',NULL,NULL,NULL,NULL,10,'guru',1),(9,'::1','is_513197','$2y$08$U6OjOMqa4yvru.SqWTqxpur8XEV1fYjGb4N7HV2X2W.bJ3iNBKevG',NULL,'is_513197@gmail.com',NULL,NULL,NULL,NULL,1609058110,1621504214,1,'Nur Cholis, S.Pdi',NULL,NULL,NULL,NULL,12,'guru',NULL),(10,'::1','is_477147','$2y$08$a7i0YglvIrBrmFSHLfkGcOLq2bXdyYw6mKQPv0fd5XAdlsGq0xcAm',NULL,'is_477147@gmail.com',NULL,NULL,NULL,NULL,1609058143,1609161762,1,'Pohet Bintoto, S.Pd., M.Si.',NULL,NULL,NULL,NULL,13,'guru',NULL),(11,'::1','is_281057','$2y$08$/sh4Fl5e6ERosaRuoY1TbeND.a2ZnIlWcecy164L.HeqgwnPN3CXe',NULL,'is_281057@gmail.com',NULL,NULL,NULL,NULL,1609058204,1609058223,1,'Mohammad Nazibullah, M.Pd.',NULL,NULL,NULL,NULL,14,'guru',1),(12,'::1','is_950864','$2y$08$.zH8VbjS1kSw.AaeZmn3fepcNXga0u1V4crBNyLotssKcA3pheME2',NULL,'is_950864@gmail.com',NULL,NULL,NULL,NULL,1609058269,1609058286,1,'Syamsul Arifin, S.Pd.',NULL,NULL,NULL,NULL,15,'guru',NULL),(13,'::1','is_64964','$2y$08$PqXqWLPoMZQxAlRCI3M4sOnRk22Hl1cr3vhTTJhrXNGpH/bQLYXFq',NULL,'is_64964@gmail.com',NULL,NULL,NULL,NULL,1609058314,1609058345,1,'M. Mirza Ayatulloh, S.Psi.',NULL,NULL,NULL,NULL,16,'guru',1),(14,'::1','is_56015','$2y$08$16KcxEJG.1uz/KgEm29SNOJ.JaujRK4gp3qKQlsKK044ZP6jcFBii',NULL,'is_56015@gmail.com',NULL,NULL,NULL,NULL,1609058398,1609141823,1,'Sutan Taufik, S.Hum',NULL,NULL,NULL,NULL,17,'guru',NULL),(15,'::1','is_427471','$2y$08$G46TnI8BorORyqzMbNB.Hu/VABmSGTtT8U6uwe2dmNXcoqmhugzam',NULL,'is_427471@gmail.com',NULL,NULL,NULL,NULL,1609058453,1609058465,1,'Fatat Alvin D. N. S.',NULL,NULL,NULL,NULL,18,'guru',NULL),(16,'::1','is_497512','$2y$08$RKIXmDz2O2TLJCi.DN3vpuLBEGRXxsu7ALt6S438WHFAJIYJDmAHq',NULL,'is_497512@gmail.com',NULL,NULL,NULL,NULL,1609058509,1609058530,1,'Nailatur Rizqiyah S.P.',NULL,NULL,NULL,NULL,19,'guru',1),(17,'::1','is_563268','$2y$08$/PsZXKIavCasI8.GHz48lugp/SKEJH0oOJaDTek4R17azsPG7HS36',NULL,'is_563268@gmail.com',NULL,NULL,NULL,NULL,1609058569,1609058580,1,'Saifudin Mansur, S.TP',NULL,NULL,NULL,NULL,20,'guru',NULL),(18,'::1','is_372371','$2y$08$/XfJfdVqXX0mE2KYNf6GfOPSPbhkd12YM9UjPIi1G1397y72JqPVq',NULL,'is_372371@gmail.com',NULL,NULL,NULL,NULL,1609058613,1609058633,1,'Faridatuz Zakiyah, S.TP',NULL,NULL,NULL,NULL,21,'guru',NULL),(19,'::1','is_242171','$2y$08$2WPrn6dI0XcyCJuP.uLZcOLrZGmWDGX1xRPoJuXAbHtvmZ3zN38ba',NULL,'is_242171@gmail.com',NULL,NULL,NULL,NULL,1609058682,1609058717,1,'Muhammad Iwan Wahyudi, S.Pd.',NULL,NULL,NULL,NULL,22,'guru',NULL),(20,'::1','is_762679','$2y$08$ZtDOeaL3wDh.2/pHqMqfRed1krO5F.qkgeQEHqXGhXuvMQxkrzKBu',NULL,'is_762679@gmail.com',NULL,NULL,NULL,NULL,1609058788,1609058797,1,'Muhammad Yasminto, S.Pd.',NULL,NULL,NULL,NULL,23,'guru',NULL),(21,'::1','is_641841','$2y$08$VaKYWH0thKssaW0N70np/et8ev9WGT52gnpH7QKkh93mdVg3Af91q',NULL,'is_641841@gmail.com',NULL,NULL,NULL,NULL,1609058827,1609923386,1,'Muhammad Ali Saifudin, S.Pd.',NULL,NULL,NULL,'5.png',24,'guru',1),(22,'::1','is_454070','$2y$08$5L1Y0/zyAbZls1E2oA83huR6S9SRORbmqY9IEFQhHZVZ7tQqjZin2',NULL,'is_454070@gmail.com',NULL,NULL,NULL,NULL,1609058904,1610152477,1,'M. Faiz Zidni Mubarok, S.Pt',NULL,NULL,NULL,NULL,25,'guru',1),(23,'::1','is_998856','$2y$08$rTEqZyBWEcSveu75gLWTHurjCs2TBYHG5..sb9Vk8qmODZmx2Sqpu',NULL,'is_998856@gmail.com',NULL,NULL,NULL,NULL,1609059002,1609059058,1,'Fahmi Jamaludin, S.T.',NULL,NULL,NULL,NULL,26,'guru',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`) USING BTREE,
  KEY `fk_users_groups_users1_idx` (`user_id`) USING BTREE,
  KEY `fk_users_groups_groups1_idx` (`group_id`) USING BTREE,
  CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,1),(3,3,3),(4,4,5),(5,5,3),(6,6,3),(7,7,3),(8,8,3),(9,9,3),(10,10,3),(11,11,3),(12,12,3),(13,13,3),(14,14,3),(15,15,3),(16,16,3),(17,17,3),(18,18,3),(19,19,3),(20,20,3),(21,21,3),(22,22,3),(23,23,3);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-06 14:12:20
