<section class="services section-bg section-space">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section-title  style2 text-center">
							<div class="section-top">
								<h1><span><?php echo $profil_website['nama_website'] ?></span><b><?php echo $setting_table['title_galery'] ?></b></h1><h4><?php echo $setting_table['subtitle_galery'] ?></h4>
							</div>
							<div class="section-bottom">
								<div class="text-style-two">
									<p><?php echo $setting_table['desc_galery'] ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<?php foreach ($galery as $key => $value): ?>
						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Service -->
							<div class="single-service">
								<div class="service-head">
									<img src="<?php echo base_url('include/media/'.$value['img']); ?>" alt="#">
									<div class="icon-bg"><i class="fa fa-handshake-o"></i></div>
								</div>
								<div class="service-content">
									<h4><a href="<?php echo base_url('frontend/galery_detail/'.$value['id_galery']); ?>"><?php echo $value['title'] ?></a></h4>
									
									<a class="btn" href="<?php echo base_url('frontend/galery_detail/'.$value['id_galery']); ?>"><i class="fa fa-arrow-circle-o-right"></i>View Pict</a>
								</div>
							</div>
							<!--/ End Single Service -->
						</div>
					<?php endforeach ?>
				</div>
				<div class="row" style="margin-top: 5%;">
					<div class="col-12">
						<div class="section-title  style2 text-center">
							<div class="section-top">
								<div class="text-style-two">
									<p><?php echo $setting_table['desc_galery_video'] ?></p>
								</div>
							</div>
							<div class="section-bottom">
								
								<h1><b><?php echo $setting_table['title_galery_video'] ?></b></h1><h4><?php echo $setting_table['subtitle_galery_video'] ?></h4>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<?php foreach ($galery_video as $key => $value): ?>
						<div class="col-lg-6 col-md-6 col-12">
							<!-- Single Service -->
							<iframe width="100%" height="435" src="<?= $value['link'] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<!--/ End Single Service -->
						</div>
					<?php endforeach ?>
				</div>
			</div>
		</section>