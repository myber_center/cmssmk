<!-- Contact Us -->
		<section class="contact-us section-space">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-12">
						<!-- Contact Form -->
						<div class="contact-form-area m-top-30">
							<h4>Get In Touch</h4>
							<iframe src="<?php echo $profil_website['location'] ?>" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
						</div>
						<!--/ End contact Form -->
					</div>
					<div class="col-lg-5 col-md-5 col-12">
						<div class="contact-box-main m-top-30">
							<div class="contact-title">
								<h2>Contact with us</h2>
								<p><?php echo $setting_table['text_contact_us'] ?></p>
							</div>
							<!-- Single Contact -->
							<div class="single-contact-box">
								<div class="c-icon"><i class="fa fa-clock-o"></i></div>
								<div class="c-text">
									<h4>Opening Hour</h4>
									<p><?php echo $profil_website['hari_buka'] ?><br><?php echo $profil_website['jam_buka'] ?></p>
								</div>
							</div>
							<!--/ End Single Contact -->
							<!-- Single Contact -->
							<div class="single-contact-box">
								<div class="c-icon"><i class="fa fa-phone"></i></div>
								<div class="c-text">
									<h4>Call Us Now</h4>
									<p>Tel.: <?php echo $profil_website['no_hp'] ?></p>
								</div>
							</div>
							<!--/ End Single Contact -->
							<!-- Single Contact -->
							<div class="single-contact-box">
								<div class="c-icon"><i class="fa fa-envelope-o"></i></div>
								<div class="c-text">
									<h4>Email Us</h4>
									<p><?php echo $profil_website['email'] ?></p>
								</div>
							</div>
							<!--/ End Single Contact -->
							<div class="button">
								<a href="<?php echo base_url('frontend/about_us') ?>" class="bizwheel-btn theme-1">About Us<i class="fa fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
		<!--/ End Contact Us -->
		