<section class="about-us section-space">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-5 col-md-6 offset-lg-1"><!-- About Video -->
			<div class="modern-img-feature"><img alt="#" src="/cmsbis/include/kcfinder/upload/images/Raw-material-process-1024x768.jpeg" style="height:100%; width:100%" />
				<div class="video-play">&nbsp;</div>
			</div>
			<!--/End About Video  --></div>
			<div class="col-12 col-lg-5 col-md-6">
				<div class="about-content default section-title text-left">
					<div class="section-top">
						<h1><strong>About Us</strong></h1>
					</div>
					<div class="section-bottom">
						<div class="text">
							<p>Coconut Charcoal World is a leading manufacturer and exporter of 100% coconut shell charcoal briquettes, from Indonesia. We control every step of the manufacturing and quality control process of our briquettes, from coconut shell selection, cleaning and carbonization all the way through to final briquetting, drying and packaging. Our production locations are situated in all the major coconut harvesting regions of Indonesia in Sumatra, Java and Sulawesi. We supply coconut shell charcoal briquettes in bulk and can ship to any destination worldwide. We can produce briquettes in any desired shape or size and tailor make packaging according to our customers exact design preferences.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="call-action" style="background: <?php echo $setting_table['ad_section_color'] ?> !important;">
	<!-- Call To Action -->
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="call-inner">
					<h2>Our Vision</h2>
					<p>We continually strive to become the number one manufacture of high quality coconut shell charcoal briquettes in Indonesia. We work constantly to expand our production base while committing to source and use only the highest grade coconut shell charcoal and other raw materials in briquette production. We commit to always using the most innovative and efficient production practices (such as in carbonization, briquetting and drying) to ensure consistently high quality product. We commit to always ensuring the highest levels of customer satisfaction through sincere and responsive communication, competitive pricing, timely delivery and consistently excellent briquette quality for all purposes.</p>
				</div>
			</div>
		</div>
	</div>
	<!--/ End Call to action -->
</section>

		<!-- Features Area -->
		<section class="features-area section-bg">
			<div class="container">
				<div class="row">
					<?php foreach ($keunggulan as $key => $value): ?>
						<div class="col-lg-3 col-md-6 col-12">
							<!-- Single Feature -->
							<div class="single-feature">
								<div class="icon-head"><i class="fa fa-podcast"></i></div>
								<h4><a href="service-single.html"><?php echo $value['keunggulan'] ?></a></h4>
								<p><?php echo $value['keterangan'] ?></p>
							</div>
							<!--/ End Single Feature -->
						</div>
					<?php endforeach ?>
					
					
				</div>
			</div>
		</section>
		<!--/ End Features Area -->
		