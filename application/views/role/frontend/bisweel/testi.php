
		
		<!-- Testimonials -->
		<section class="testimonials section-space" style="background-image:url(<?php echo base_url('include/media/'.$setting_table['testi_section_img']) ?>)">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-9 col-12">
						<div class="section-title default text-left">
							<div class="section-top">
								<h1 style="color:<?php echo $setting_table['testi_section_head_color'] ?> !important"><b><?php echo $setting_table['testi_section_head_text'] ?></b></h1>
							</div>
						</div>
						<div class="testimonial-inner">
							<div class="testimonial-slider">
								<!-- Single Testimonial -->
								<?php foreach ($testimoni as $key => $value): ?>
									<div class="single-slider">
										<ul class="star-list">
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<p><?php echo $value['testimoni'] ?></p>
										<!-- Client Info -->
										<div class="t-info">
											<div class="t-left">
												<div class="client-head"><img src="<?php echo base_url('include/media/'.$value['img']) ?>" alt="#"></div>
												<h2><?php echo $value['nama'] ?><span><?php echo $value['keterangan'] ?></span></h2>
											</div>
											<div class="t-right">
												<div class="quote"><i class="fa fa-quote-right"></i></div>
											</div>
										</div>
									</div>

								<?php endforeach ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Testimonials -->