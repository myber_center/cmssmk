
		<!-- Features Area -->
		<section class="features-area section-bg">
			<div class="container">
				<div class="row">
					<?php foreach ($keunggulan as $key => $value): ?>
						<div class="col-lg-3 col-md-6 col-12">
							<!-- Single Feature -->
							<div class="single-feature">
								<div class="icon-head"><i class="fa fa-podcast"></i></div>
								<h4><a href="service-single.html"><?php echo $value['keunggulan'] ?></a></h4>
								<p><?php echo $value['keterangan'] ?></p>
							</div>
							<!--/ End Single Feature -->
						</div>
					<?php endforeach ?>
					
					
				</div>
			</div>
		</section>
		<!--/ End Features Area -->
		