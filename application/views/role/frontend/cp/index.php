<section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h1>Milenial Indonesia Bangkit</h1>
                            <p>Indonesia bangkit adalah sebuah gagasan pembentukan badan yang bersifat independen. tujuan dari Indonesia bangkit tidak lain adalah untuk menciptakan kekuatan ekonomi mandiri bagi bangsa Indonesia dalam berkompetisi di pasar internasional.</p>
                            <!-- <a href="#" class="btn_2 banner_btn_1">Get Started </a>
                            <a href="#" class="btn_2 banner_btn_2">Sign up for free </a> -->
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="banner_img d-none d-lg-block">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>banner_img_new.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->
<!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-6 col-lg-5">
                    <div class="about_us_text">
                        <h2>Milenial Indonesia Bangkit</h2>
                        <p>Dengan berfokus pada kemampuan generasi penerus (Millenial) dan juga kekayaan alam di Indonesia, badan ini berusaha untuk bisa menciptakan sebuah ekosistem bisnis yang akan membawa Indonesia menjadi negaea dengan ekonomi terbesar no. 4 di dunia secara nyata dan lebih cepat</p>
                        <a href="#" class="btn_1">Selengkapnya</a>
                        <a href="#" class="btn_2">Join bersama kami</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>mib.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <img src="<?php echo base_url('include/template/sasu/img/')?>left_sharp.png" alt="" class="left_shape_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>about_shape.png" alt="" class="about_shape_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>animate_icon/Shape-16.png" alt="" class="feature_icon_1">
        <img src="<?php echo base_url('include/template/sasu/img/')?>animate_icon/Shape-1.png" alt="" class="feature_icon_4">
    </section>