

    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Pengelolaan dan Badan Kepengurusan</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pen1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pen2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pen3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>pen4.png" alt="">
                    </div>
                </div>
            </div>
            
            
        </div>
       
    </section>
    <!-- about_us part end-->
