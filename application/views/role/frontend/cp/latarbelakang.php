
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Latar Belakang</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!-- about_us part start-->
    <section class="about_us section_padding">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>4.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>5.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>6.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>7.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>8.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>9.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-md-12 col-lg-12">
                    <div class="learning_img">
                        <img src="<?php echo base_url('include/template/sasu/img/')?>10.png" alt="">
                    </div>
                </div>
            </div>
        </div>
       
    </section>
    <!-- about_us part end-->
