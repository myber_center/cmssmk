

	<!-- Footer -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="footer_news">
						<h3>Find Us</h3>
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15805.799487370541!2d112.6320909!3d-7.9523748!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9783724da5089751!2sLowokwaru%204%20Elementary%20School%20Malang!5e0!3m2!1sid!2sid!4v1639968380982!5m2!1sid!2sid" width="100%" height="450" style="border:0;" id="map" allowfullscreen="" loading="lazy"></iframe>
					</div>
				</div>
			</div>
			<!-- Back To Top -->
			<a href="#top" class="scroll"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
			<!-- /#Back To Top -->
			<div class="footer_bottom">
				<div class="row">
					<div class="col-md-7">
						<div class="copyright">
							<p>Copyright 2021 - Myber.  All Rights Reserved.</a>
							</p>
						</div>
					</div>
					<div class="col-md-5">
						<ul class="socialicons">
							<li><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							</li>
							<li><a href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							</li>
							<li><a href="javascript:void(0)"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
							</li>
							<li><a href="javascript:void(0)"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</footer>
	<!-- /#Footer -->
