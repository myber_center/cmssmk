
<!--  Services Start  -->
<section id="services" class="padding_bottom padding_top">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center heading">
				<h2><?= $sec3_title ?></h2>
				<p class="heading_space"><?= $sec3_text ?></p>
			</div>
		</div>
		<div class="row">
			<div id="services_slider">
				<?php $no = 0; ?>
				<?php foreach ($fasilitas as $value): ?>
					<div class="item services_box">
						<div class="services_img">
							<img src="<?php echo base_url('include/media/'.$value['foto'])?>" alt="Owl Image">
						</div>
						<div class="services_detail">
							<span><?= (++$no); ?></span>
							<h3><?= $value['nama'] ?></h3>
							<p><?= $value['keterangan'] ?></p>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</section>
<!--  Services End  -->
