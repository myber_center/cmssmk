<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Nama Produk (Title):</label>
	              <div class="col-lg-6">
	              	<input type="hidden" name="id_produk" value="<?php echo $data_get['data_edit']['id_produk'] ?>">
	                <input type="text" name="nama" value="<?php echo $data_get['data_edit']['nama'] ?>" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Deskripsi (Description):</label>
	              <div class="col-lg-9">
                	<textarea name="deskripsi"  required id="deskripsi"><?php echo $data_get['data_edit']['deskripsi'] ?></textarea>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Harga (Price):</label>
	              <div class="col-lg-6">
	                <input type="number"name="harga" value="<?php echo $data_get['data_edit']['harga'] ?>" required class="form-control" required placeholder="Input here......">
	                <br>
	                <input type="checkbox" value="<?php echo $data_get['data_edit']['is_price'] ?>" <?php echo ($data_get['data_edit']['is_price'] == 0) ? "checked":""; ?> name="is_price"> Jangan tampilkan harga
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Foto Produk (Cover):</label>
	              <div class="col-lg-6">
	                <input type="file" name="foto"  class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Kategori (Category):</label>
	              <div class="col-lg-6">
	              	<select class="form-control" required  name="idkategoriproduk_fk">
	              		<?php foreach ($data_get['kategori_produk'] as $value): ?>
	              			<option value="<?= $value['id_kategori_produk'] ?>" <?php echo ($data_get['data_edit']['idkategoriproduk_fk'] == $value['id_kategori_produk']) ? "selected" : "" ; ?>><?= $value['kategori_produk'] ?></option>
	              		<?php endforeach ?>
	              	</select>
	              </div>
	            </div>
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>