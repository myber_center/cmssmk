<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
				<input type="hidden" name="id_testimoni" value="<?php echo $data_get['data_edit']['id_testimoni'] ?>">
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Nama:</label>
	              <div class="col-lg-6">
	                <input type="text" name="nama" class="form-control" value="<?php echo $data_get['data_edit']['nama'] ?>" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Keterangan/Jabatan:</label>
	              <div class="col-lg-9">
	              	<input type="text" name="keterangan" class="form-control" value="<?php echo $data_get['data_edit']['keterangan'] ?>"  required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Testimoni:</label>
	              <div class="col-lg-9">
	              	<input type="text" name="testimoni" class="form-control" value="<?php echo $data_get['data_edit']['testimoni'] ?>"  required placeholder="Input here......">
	              </div>
	            </div>
	           
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Foto (Cover):</label>
	              <div class="col-lg-6">
	                <input type="file" name="img"  class="form-control"  placeholder="Input here......">
	              </div>
	            </div>
	            
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>