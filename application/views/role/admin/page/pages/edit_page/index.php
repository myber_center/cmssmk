<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
				<input type="hidden" value="<?php echo $data_get['data_edit']['id_pages'] ?>" name="id_pages">
				<div class="form-group">
	              <label class="col-lg-3 control-label">Link (Link):</label>
	              <div class="col-lg-4">
	                <input type="text" name="" readonly="" value="<?php echo base_url('frontend/pages/') ?>" required class="form-control" required placeholder="Input here......">
	              </div>
	              <div class="col-lg-4">
	                <input type="text" name="link" value="<?php echo $data_get['data_edit']['link'] ?>" required class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Judul (Title):</label>
	              <div class="col-lg-6">
	                <input type="text" name="title" value="<?php echo $data_get['data_edit']['title'] ?>" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Konten (content):</label>
	              <div class="col-lg-9">
                	<textarea name="content"  required id="content"><?php echo $data_get['data_edit']['content'] ?></textarea>
	              </div>
	            </div>
	            
	            
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>