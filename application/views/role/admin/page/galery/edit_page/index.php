<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Title:</label>
	              <div class="col-lg-6">
	              	<input type="hidden" name="id_galery" value="<?php echo $data_get['data_edit']['id_galery'] ?>">
	                <input type="text" name="title" value="<?php echo $data_get['data_edit']['title'] ?>" class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Deskripsi (Description):</label>
	              <div class="col-lg-9">
                	<textarea name="desc"  required id="deskripsi"><?php echo $data_get['data_edit']['desc'] ?></textarea>
	              </div>
	            </div>
	            
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Foto  (Cover):</label>
	              <div class="col-lg-6">
	                <input type="file" name="img"  class="form-control"  placeholder="Input here......">
	              </div>
	            </div>
	            
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>