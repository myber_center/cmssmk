
<form class="form-horizontal" action="<?php echo $data_get['param']['table'] ?>/update_data" id="app-submit" method="POST">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-body">
			<fieldset>
				<div class="form-group">
	              <label class="col-lg-3 control-label">Judul (Title):</label>
	              <div class="col-lg-6">
	                <input type="text" value="<?php echo $data_get['data_edit']['title'] ?>" name="title"  required class="form-control" required placeholder="Input here......">
	              </div>
	            </div>
	           <div class="form-group">
	              <label class="col-lg-3 control-label">Slider:</label>
	              <div class="col-lg-6">
	              	<input type="hidden" value="<?=  $data_get['data_edit']['id_slider'] ?>" name="id_slider">
	                <input type="file" name="img"  class="form-control" placeholder="Input here......">
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-lg-3 control-label">Deskripsi (Description):</label>
	              <div class="col-lg-9">
                	<textarea name="deskripsi" class="form-control" required id="deskripsi"><?= $data_get['data_edit']['deskripsi'] ?></textarea>
	              </div>
	            </div>
	            <button class="btn btn-success" type="submit">Simpan</button>
            </fieldset>	
		</div>
	</div>
</div>
</form>