<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= $profil_website['nama_website'] ?></title>

<!-- Bootstrap -->
<link href="<?php echo base_url('include/template/edublog/css/')?>bootstrap.min.css" rel="stylesheet">

<!-- Font-awesome -->
<link href="<?php echo base_url('include/template/edublog/css/')?>font-awesome.min.css" rel="stylesheet">

<!-- Bootsnav -->
<link href="<?php echo base_url('include/template/edublog/css/')?>bootsnav.css" rel="stylesheet">

<!-- Cubeportfolio -->
<link href="<?php echo base_url('include/template/edublog/css/')?>cubeportfolio.min.css" rel="stylesheet">

<!-- OWL-Carousel -->
<link href="<?php echo base_url('include/template/edublog/css/')?>owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url('include/template/edublog/css/')?>owl.transitions.css" rel="stylesheet">

<!-- Slider -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('include/template/edublog/css/')?>settings.css">

<!-- Custom Style Sheet -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('include/template/edublog/css/')?>style.css">


<link rel="shortcut icon" href="<?php echo base_url('include/media/'.$profil_website['icon'])?>">


</head>
