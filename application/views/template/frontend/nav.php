<header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.html"> <img src="<?php echo base_url('include/template/sasu/img/')?>logo_mib.png" style="max-width: 150px;max-height:60px; " alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu_icon"><i class="fas fa-bars"></i></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('frontend/'); ?>">Home</a>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="feature.html">Latar Belakang</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="pricing.html">Visi dan Misi</a>
                                </li> -->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Tentang Kami
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/latarbelakang'); ?>"> Latar Belakang</a>
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/potensi'); ?>"> Potensi Indonesia</a>
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/mib'); ?>"> Milenial Indonesia Bangkit</a>
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/visi'); ?>">Visi dan Misi</a>
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/pelaku'); ?>">Pelaku Dalam Badan MIB</a>
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/sumberdana'); ?>">Sumber Pendanaan</a>
                                        <a class="dropdown-item" href="<?php echo base_url('frontend/bagan'); ?>">Pengelolaan dan Bagan Kepengurusan</a>
                                    </div>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('frontend/target'); ?>">Target Akhir</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('frontend/penyusun'); ?>">Penyusun</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact</a>
                                </li>
                            </ul>
                        </div>
                        <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>
                        <div class="dropdown cart">
                            <!-- <a class="dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cart-plus"></i>
                            </a> -->
                            <!-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div class="single_product">

                                </div>
                            </div> -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="search_input" id="search_input_box">
            <div class="container ">
                <form class="d-flex justify-content-between search-inner">
                    <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                    <button type="submit" class="btn"></button>
                    <span class="ti-close" id="close_search" title="Close Search"></span>
                </form>
            </div>
        </div>
    </header>
    <!-- Header part end-->
