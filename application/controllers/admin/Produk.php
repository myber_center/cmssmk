<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class produk extends MY_Controller {

	public $arr = [
			'title'				=>	'Halaman produk',
			'table'				=>	'produk',
			'column'			=>	[ 'nama','deskripsi','harga','foto','is_price','idkategoriproduk_fk'],
			'column_order'		=>	[ 'id_produk','nama','deskripsi','harga','foto','is_price','idkategoriproduk_fk'],
			'column_search'		=>	[ 'id_produk','nama','deskripsi','harga','foto','is_price','idkategoriproduk_fk'],
			'order'				=>	['id_produk'	=>	'DESC'],
			'id'				=>	'id_produk'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/produk/index_page/index','role/admin/page/produk/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$data['kategori_produk']	=	$this->my_where('kategori_produk',[])->result_array();
		$this->my_view(['role/admin/page/produk/add_page/index','role/admin/page/produk/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;

			$data['kategori_produk']	=	$this->my_where('kategori_produk',[])->result_array();
			$this->my_view(['role/admin/page/produk/edit_page/index','role/admin/page/produk/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
			$foto = $this->save_media([
				'path'	=>	"./include/media/",
				'filename' => 'foto',
			]);
		}

		$data = [
			'nama'					=>	$_POST['nama'],
			'deskripsi'				=>	$_POST['deskripsi'],	
			'harga'					=>	$_POST['harga'],		
			'idkategoriproduk_fk'	=>	$_POST['idkategoriproduk_fk'],
			'is_price'				=>	((isset($_POST['is_price'])) ? $_POST['is_price'] : 1) 
		];
		if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
				$data['foto'] = ((isset($foto)) ? $foto['file_name'] : '');
		}

		$this->save_data('produk', $data);
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
					$foto = $this->save_media([
						'path'	=>	"./include/media/",
						'filename' => 'foto',
					]);
				}
		$data = [
			'nama'					=>	$_POST['nama'],
			'deskripsi'				=>	$_POST['deskripsi'],	
			'harga'					=>	$_POST['harga'],		
			'idkategoriproduk_fk'	=>	$_POST['idkategoriproduk_fk'],
			'is_price'				=>	((isset($_POST['is_price'])) ? $_POST['is_price'] : 1) 
		];

			if(file_exists($_FILES['foto']['tmp_name']) || is_uploaded_file($_FILES['foto']['tmp_name'])) {
				$data['foto'] = ((isset($foto)) ? $foto['file_name'] : '');
			}
			$this->my_update(
				'produk', 
				$data,
				['id_produk'=>$_POST['id_produk']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_produk'].'"></input>';
            $row[]		=	'<img src="'.(base_url('include/media/'.$field['foto'])).'" style="width:40px;height:40px;"> <a class="app-item" href="produk/edit_page/'.$field['id_produk'].'">'. $field['nama'].'</a>';
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}