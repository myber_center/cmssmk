<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class setting_template extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/
	public $arr = [
			'title'				=>	'Halaman setting_template',
			'table'				=>	'setting_template',
			'column'			=>	[ 'title','tag','setting_template','img','create_at','idblogcategory_fk','visit_count'],
			'column_order'		=>	[ 'id_blog_post','title','tag','setting_template','img','create_at','idblogcategory_fk','visit_count'],
			'column_search'		=>	[ 'id_blog_post','title','tag','setting_template','img','create_at','idblogcategory_fk','visit_count'],
			'order'				=>	['setting_template'	=>	'DESC'],
			'id'				=>	'setting_template'
	];

	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$data['setting_template']		=	$this->my_where('setting_table',['table'=>'bisweel'])->result_array();

		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/setting_template/index','role/admin/page/setting_template/js_setting_template'],$data);
	}

	/*
		ADD DATA
	*/


	function update_data()
	{
		$data = [];
		foreach ($this->my_where('setting_table',['table'=>'bisweel'])->result_array() as $key => $value) {
			if ($this->my_update('setting_table',['value' => $_POST['s'.$value['id_setting_table']], 'value_en'=>$_POST['s_en'.$value['id_setting_table']]],['name'=>$value['name']])) {
				
			}	else 	{
				echo "error";
			}
		}
		

	}

	/*
		DELETE DATA
	*/

}