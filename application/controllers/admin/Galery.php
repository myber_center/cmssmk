<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class galery extends MY_Controller {

	public $arr = [
			'title'				=>	'Halaman galery',
			'table'				=>	'galery',
			'column'			=>	[ 'img','title','desc'],
			'column_order'		=>	[ 'id_galery','img','title','desc'],
			'column_search'		=>	[ 'id_galery','img','title','desc'],
			'order'				=>	['id_galery'	=>	'DESC'],
			'id'				=>	'id_galery'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/galery/index_page/index','role/admin/page/galery/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/galery/add_page/index','role/admin/page/galery/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;

			$this->my_view(['role/admin/page/galery/edit_page/index','role/admin/page/galery/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
			$img = $this->save_media([
				'path'	=>	"./include/media/",
				'filename' => 'img',
			]);
		}

		$data = [
			'title'				=>	$_POST['title'],	
			'desc'				=>	$_POST['desc'],		
		];
		if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
				$data['img'] = ((isset($img)) ? $img['file_name'] : '');
		}

		$this->save_data('galery', $data);
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
					$img = $this->save_media([
						'path'	=>	"./include/media/",
						'filename' => 'img',
					]);
				}
		$data = [
			'title'					=>	$_POST['title'],
			'desc'				=>	$_POST['desc'],	
		];

			if(file_exists($_FILES['img']['tmp_name']) || is_uploaded_file($_FILES['img']['tmp_name'])) {
				$data['img'] = ((isset($img)) ? $img['file_name'] : '');
			}
			$this->my_update(
				'galery', 
				$data,
				['id_galery'=>$_POST['id_galery']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_galery'].'"></input>';
            $row[]		=	'<img src="'.(base_url('include/media/'.$field['img'])).'" style="width:40px;height:40px;"> <a class="app-item" href="galery/edit_page/'.$field['id_galery'].'">'. $field['title'].'</a>';
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}