<?php
defined('BASEPATH') OR exit('No direct script access allowed');
		use PhpOffice\PhpSpreadsheet\Spreadsheet;
		use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class pages extends MY_Controller {
	

	public $arr = [
			'title'				=>	'Halaman pages',
			'table'				=>	'pages',
			'column'			=>	[ 'content', 'title','link','visit_count', 'cover'],
			'column_order'		=>	[ 'id_pages','content','title','link','visit_count', 'cover'],
			'column_search'		=>	[ 'id_pages','content','title','link','visit_count', 'cover'],
			'order'				=>	['id_pages'	=>	'DESC'],
			'id'				=>	'id_pages'
	];

	/*
		CHANGE PAGE
	*/
	public function get_data()
	{
		/*if you need custom page*/

		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/pages/index_page/index','role/admin/page/pages/index_page/js'],$data);

	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$data['param'] 		= 	$this->arr;
		$this->my_view(['role/admin/page/pages/add_page/index','role/admin/page/pages/add_page/js'],$data);
	}

	public function edit_page($id)
	{
		$dt = $this->arr;

		$data['param'] 		= 	$this->arr;
		if (isset($id)) {
			$data_set = $this->my_where($dt['table'],[$dt['id']=>$id])->row_array();
			$data['data_edit']	=	$data_set;
			$this->my_view(['role/admin/page/pages/edit_page/index','role/admin/page/pages/edit_page/js'],$data);
		} else {
			$this->get_data();
		}
	}

	/*
		ADD DATA 
	*/


	public function simpan_data()
	{	
		$data = [
			'title'					=>	$_POST['title'],
			'link'					=>	$_POST['link'],	
			'content'				=>	$_POST['content'],
		];

		$this->save_data('pages', $data);
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		if (isset($_POST)) {
			$data = [
				'title'					=>	$_POST['title'],
				'link'					=>	$_POST['link'],	
				'content'				=>	$_POST['content'],
				];

			$this->my_update(
				'pages', 
				$data,
				['id_pages'=>$_POST['id_pages']]
			);
		}
	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		$dt = $this->arr;
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete($dt['table'],[$dt['id']=>$value]);
		}
	}


	public function datatable()
	{
		$_POST['frm']   =   $this->arr;
        $list           =   $this->mod_datatable->get_datatables();
        $data           =   array();
        $no             =   $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row        =   array();
            
            $row[]      =   '<input type="checkbox" name="get-check" value="'.$field['id_pages'].'"></input>';
            $row[]		=	$field['title'];
            $row[]		=	$field['link'];
            $data[]     =   $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );

        echo json_encode($output);
	}
	
	
}